#This file demostrated my git repository

tyler@powell-lap:~/.git/lab1$ git log --all --decorate --graph --oneline
* ec81c96 (HEAD, master) this commit is part of my unmerged branch
* bdc8cf9 (origin/master, Unmerged) First commit. Adding a README.
*   1b7080d (origin/branch1, branch1) Merge branch 'branch2' into branch1
|\  
| * 35529c4 (origin/branch2, branch2) modifed file2.txt within branch2
| * dbc9b90 (origin/branch3, branch3) modified file2.txt
| * 14dfb5a commiting file2.txt
* |   f77b322 Merge branch 'master' into branch1
|\ \  
| * | c2f2e58 commiting changes made to README file
| * |   3205f10 Merge branch 'branch1'
| |\ \  
| | |/  
| * | 0f737b0 added lab1.txt to master
| * | 0e1d8fe commit the README file
* | | 2ed0efc made a change to file1.txt in branch1
| |/  
|/|   
* | 8401f50 merging the change i made to file1.txt
|/  
* f0613ea added more txt
* de8f7fd this is the first file

